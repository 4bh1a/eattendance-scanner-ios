//  HOMEVC.swift
//  Attendance
//
//  Created by Abhishek babladi on 19/07/19.
//  Copyright © 2019 Abhishek babladi. All rights reserved.
//

import UIKit
import Lottie
import SystemConfiguration
class HomeViewController: UIViewController,UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var qrBtn: UIButton!
    let transition = CircularTransition()
    
    private let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setReachabilityNotifier()
//        qrBtn.layer.cornerRadius = qrBtn.frame.size.width / 2
//        // Do any additional setup after loading the view.
//        let successAnimationView = AnimationView(name: "qrcode")
//        successAnimationView.center = self.view.center
//        successAnimationView.frame.origin.y -= -75
//        self.view.addSubview(successAnimationView)
//        successAnimationView.play { (finished) in
//            self.view.bringSubviewToFront(self.qrBtn)
//        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let secondVC = segue.destination as! QRScannerViewController
        secondVC.transitioningDelegate = self
        secondVC.modalPresentationStyle = .custom
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = qrBtn.center
        
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .dismiss
        transition.startingPoint = qrBtn.center
        return transition
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func isNetworkReachable(with flags: SCNetworkReachabilityFlags) -> Bool {
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        let canConnectAutomatically = flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic)
        let canConnectWithoutUserInteraction = canConnectAutomatically && !flags.contains(.interventionRequired)
        return isReachable && (!needsConnection || canConnectWithoutUserInteraction)
    }
    
    private func setReachabilityNotifier () {
        //declare this inside of viewWillAppear
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        
        switch reachability.connection {
        case .wifi:
            print("Reachable via WiFi")
             qrBtn.isEnabled = true
        case .cellular:
            print("Reachable via Cellular")
             qrBtn.isEnabled = true
        case .none:
            print("Network not reachable")
             qrBtn.isEnabled = false
             self.alert(message:"No Internet connection",title: "Enable Wifi")
        }
    }
    
}

extension HomeViewController
{
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        
        DispatchQueue.main.async  {
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
