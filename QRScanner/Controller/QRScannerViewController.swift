
import UIKit
import Alamofire
import Lottie
class QRScannerViewController: UIViewController {
    
    var parameterTime = String()
    var currentDate = String()
    var monthString = String()
    var Year = Int()
    var outTime = String()
    var attendanceStatus = String()
    var totalHours = String()
    var onTime = Bool()
    var isLate = Bool()
    var month = Int()
    var day = Int()
    var hour = Int()
    var min = Int()
    var sec = Int()
    var firstCheckin = Bool()
    var temptoken = String()
    var savedDate = String()
    var stored = [String]()
    var id = Int()
    var emptyArray = [String]()
    let defaults = UserDefaults.standard
    let delegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var imgGreen: UIImageView!
    @IBOutlet weak var lblGreen: UILabel!
    @IBOutlet weak var imgRed: UIImageView!
    @IBOutlet weak var lblRed: UILabel!
    
    @IBOutlet weak var bossGreen: UIImageView!
    @IBOutlet weak var lblWelcome: UILabel!
    
    
    @IBOutlet weak var scannerView: QRScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }
    
    var qrData: QRData? = nil {
        didSet {
            if qrData != nil {
                print(" ")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        firstCheckin = defaults.bool(forKey: "FirstCheckin")
        
        if firstCheckin == true{
            dateAndTime()
            defaults.set(emptyArray, forKey: "TOKENARRAY")
            defaults.synchronize()
            defaults.set(false, forKey: "FirstCheckin")
            defaults.synchronize()
            firstCheckin = defaults.bool(forKey: "FirstCheckin")
            print(firstCheckin)
            
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !scannerView.isRunning {
            scannerView.startScanning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }
    
    
    func dateAndTime() {
        // get the current date and time
        let currentDateTime = Date()
        
        // get the user's calendar
        let userCalendar = Calendar.current
        
        // choose which date and time components are needed
        let requestedComponents: Set<Calendar.Component> = [
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second
            
        ]
        
        // get the components
        let dateTimeComponents = userCalendar.dateComponents(requestedComponents, from: currentDateTime)
        
        // now the components are available
        Year =  dateTimeComponents.year!
        month =  dateTimeComponents.month!
        day =   dateTimeComponents.day!
        hour =   dateTimeComponents.hour!
        min = dateTimeComponents.minute!
        sec =  dateTimeComponents.second!
        
        let date = Date()
        monthString = date.month
        parameterTime = ("\(hour):\(min):\(sec)")
        currentDate = ("\(day)-\(month)-\(Year)")
        defaults.set(currentDate, forKey: "CHECKDATE")
        defaults.synchronize()
    }
    
    func patchAttAPI(token:String){
        
        let outTime = parameterTime
        let currentId = id
        let    headers = [
            "Authorization": "Token \(token)"
        ]
        let parameters : Parameters = ["out_time":"\(outTime)"]
        
        
        if !Reachability.isConnectedToNetwork(){
            print("Internet Connection not Available!")
            self.alert(message:"The Internet appears to be offline",title: "")
            return
            }
        else{
            
            print("Internet Connection Available!")
            Alamofire.request("https://thawing-dawn-25703.herokuapp.com/api/attendance/\(currentId)/",method: .patch, parameters: parameters,headers:headers).responseJSON { response in
                print("Result: \(response.result)")
                if let json = response.result.value as? [String:Any] {
                    print(json)
                    self.patchCase()
                }
            }
        }
        
    }
    
    func punchAttApi(token:String){
        
        let current = parameterTime
        let staticIntime = "09:00:59"
        let inTimeCompare = current.compare(staticIntime, options: .numeric)
        
        
        if inTimeCompare == .orderedSame {
            // intime == static time
            attendanceStatus = "PR"
            outTime = "0:0:0"
            onTime = true
            isLate = false
//            punchAnimation()
            
            
        } else if inTimeCompare == .orderedAscending {
            // intime < statictime
            attendanceStatus = "PR"
            outTime = "0:0:0"
            onTime = true
            isLate = false
//            punchAnimation()
            
        }
            
        else if inTimeCompare == .orderedDescending {
            // execute if intime > statictime
            attendanceStatus = "LT"
            outTime = "0:0:0"
            onTime = false
            isLate = true
//            punchAnimation()
            
        }
        
        let    headers = [
            "contentType": "application/json",
            "Authorization": "Token \(token)"
        ]
        let parameters : Parameters = [ "year": "\(Year)",
            "month": "\(monthString)",
            "attendance_date": "\(currentDate)",
            "attendance_status": "\(attendanceStatus)",
            "in_time": "\(parameterTime)",
            "out_time": "\(outTime)",
            "total_hours": "",
            "is_late": isLate,
            "paid_leave": "",
            "reason": ""]
        
        Alamofire.request("https://thawing-dawn-25703.herokuapp.com/api/attendance/",method: .post, parameters: parameters,headers:headers).responseJSON { response in
            print("Result: \(response.result)")
            
            if let json = response.result.value as? [String:Any] {
                print(json)
                self.punchAnimation()
            }
            //Network Error Handle
            else{
                 self.alert(message:"The Internet appears to be offline",title: "")
            }
            
        }
    }
    
//    func bossPunchApi(token:String){
//
//        let current = parameterTime
//        let staticIntime = "10:30:00"
//        let inTimeCompare = current.compare(staticIntime, options: .numeric)
//
//
//        if inTimeCompare == .orderedSame {
//            // intime == static time
//            attendanceStatus = "PR"
//            outTime = "0"
//            onTime = true
//            lblWelcome.isHidden = false
//            bossGreen.isHidden  = false
//            BossCase()
//
//
//        } else if inTimeCompare == .orderedAscending {
//            // intime < statictime
//            attendanceStatus = "PR"
//            outTime = "0"
//            onTime = true
//            lblWelcome.isHidden = false
//            bossGreen.isHidden  = false
//            BossCase()
//
//        }
//
//        else if inTimeCompare == .orderedDescending {
//            // execute if intime > statictime
//            attendanceStatus = "LT"
//            outTime = "0"
//            onTime = false
//            lblBossLT.isHidden = false
//            bossRed.isHidden  = false
//            BossCase()
//
//        }
//
//        let    headers = [
//            "contentType": "application/json",
//            "Authorization": "Token \(token)"
//        ]
//        let parameters : Parameters = [ "year": "\(Year)",
//            "month": "\(monthString)",
//            "attendance_date": "\(currentDate)",
//            "attendance_status": "\(attendanceStatus)",
//            "in_time": "\(parameterTime)",
//            "out_time": "\(outTime)",
//            "total_hours": "",
//            "is_late": false,
//            "paid_leave": "",
//            "reason": ""]
//
//        Alamofire.request("https://thawing-dawn-25703.herokuapp.com/api/attendance/",method: .post, parameters: parameters,headers:headers).responseJSON { response in
//            print("Result: \(response.result)")
//
//            if let json = response.result.value as? [String:Any] {
//                print(json)
//
//            }
//
//        }
//    }
    
    
    func conditionCheckForEach(){
        
        savedDate = defaults.string(forKey: "CHECKDATE")!
        print(savedDate)
        dateAndTime()
        print(currentDate)
        
        
        if savedDate != currentDate{
            defaults.set(emptyArray, forKey: "TOKENARRAY")
            defaults.synchronize()
        }
        
        stored = defaults.array(forKey: "TOKENARRAY") as? [String] ?? []
        print(stored)
        
        temptoken = qrData!.codeString!
        
        
        
        if stored == [] {
            
            delegate.employeeTokenArray.append(temptoken)
            print(delegate.employeeTokenArray)
            
            defaults.set(delegate.employeeTokenArray, forKey: "TOKENARRAY")
            defaults.synchronize()
            punchAttApi(token: temptoken)
        }
        else{
            
            if stored.contains(temptoken){
                getIdAPI()
                
            }
            else{
                
                stored.append(temptoken)
                print(stored)
                defaults.set(stored, forKey: "TOKENARRAY")
                defaults.synchronize()
                punchAttApi(token: temptoken)
            }
            
        }
    }
    
    func getIdAPI(){
        print(temptoken)
        
        let    headers = [
            "Authorization": "Token \(temptoken)"
        ]
        Alamofire.request("https://thawing-dawn-25703.herokuapp.com/api/attendance/",method:.get,headers:headers).responseJSON { response in
            print("Result: \(response.result)")

            
            if let json = response.result.value as? Array<Dictionary<String,Any>> {
                let profileArray = json
                print(profileArray)
                
                let dict = profileArray[0]
                self.id = (dict["id"] as? Int)!
                print(self.id)
                self.patchAttAPI(token: self.temptoken)
            }
            
        }
    }
    
    func punchAnimation(){
        if  onTime == true{
            self.successCase()
            lblGreen.isHidden = false
            imgGreen.isHidden  = false
        }
        else{
            self.lateCase()
            lblRed.isHidden = false
            imgRed.isHidden = false
        }
        
    }
    
    func successCase(){
        let successAnimationView = AnimationView(name: "unlocked")
        successAnimationView.frame = CGRect(x: 0, y: 0, width: 500, height: 500)
        successAnimationView.center = self.view.center
        successAnimationView.contentMode = .scaleAspectFill
        self.view.addSubview(successAnimationView)
        successAnimationView.play { (finished) in
            /// Animation finished
            self.view.sendSubviewToBack(successAnimationView)
            self.lblGreen.isHidden = true
            self.imgGreen.isHidden  = true
            Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: { _ in self.dismiss(animated: true, completion: nil)} )
        }
        
    }
    func lateCase(){
        let successAnimationView = AnimationView(name: "redlock")
        successAnimationView.frame = CGRect(x: 0, y: 0, width: 500, height: 500)
        successAnimationView.center = self.view.center
        successAnimationView.contentMode = .scaleAspectFill
        self.view.addSubview(successAnimationView)
        successAnimationView.play { (finished) in
            /// Animation finished
            self.view.sendSubviewToBack(successAnimationView)
            self.lblRed.isHidden = false
            self.imgRed.isHidden = false
            Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: { _ in
                self.dismiss(animated: true, completion: nil)} )
        }
    }
    
    func patchCase(){
        lblWelcome.isHidden = false
        bossGreen.isHidden = false
        let successAnimationView = AnimationView(name: "patch")
        successAnimationView.frame = CGRect(x: 0, y: 0, width: 500, height: 500)
        successAnimationView.center = self.view.center
        successAnimationView.contentMode = .scaleAspectFill
        self.view.addSubview(successAnimationView)
        successAnimationView.play { (finished) in
            /// Animation finished
            self.view.sendSubviewToBack(successAnimationView)
            self.lblWelcome.isHidden = true
            self.bossGreen.isHidden = true
            Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: { _ in self.dismiss(animated: true, completion: nil)} )
        }
    }
    
//    func BossCase(){
//        let successAnimationView = AnimationView(name: "welcomeBoss1")
//        successAnimationView.frame = CGRect(x: 0, y: 0, width: 400, height: 400)
//        successAnimationView.center = self.view.center
//        successAnimationView.contentMode = .scaleAspectFill
//        self.view.addSubview(successAnimationView)
//        successAnimationView.play { (finished) in
//            /// Animation finished
//            self.view.sendSubviewToBack(successAnimationView)
//            self.lblWelcome.isHidden = true
//            self.bossGreen.isHidden  = true
//            Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: { _ in
//                self.dismiss(animated: true, completion: nil)} )
//        }
//    }
}


extension QRScannerViewController: QRScannerViewDelegate {
    func qrScanningDidStop() {}
    
    func qrScanningDidFail() {
        presentAlert(withTitle: "Error", message: "Scanning Failed. Please try again")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        self.qrData = QRData(codeString: str)
        conditionCheckForEach()
    }
    
    
    
}


extension QRScannerViewController {}


extension Date {
    var month: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        return dateFormatter.string(from: self)
    }
}

extension QRScannerViewController
{
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        
        DispatchQueue.main.async  {
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
